﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public void createTable(DataGridView dgv)
        {
            int n = int.Parse(textBox1.Text);
            for (int i = 0; i < 4; i++)
            {
                dgv.Columns.Add("", "");
            }

            for (int i = 0; i < n; i++)
            {
                dgv.Rows.Add();
            }
        }

        private void TableCreateClick(object sender, EventArgs e)
        {
            createTable(dataGridView1);
            createTable(dataGridView2);
            groupBox1.Visible = true;
            groupBox2.Visible = false;
        }

        private void SortClick(object sender, EventArgs e)
        {
            int n = Convert.ToInt32(textBox1.Text);
            int square = int.Parse(textBox2.Text);
            Sort ST = new Sort();
            Plitka[] Arr  = new Plitka[n];
            ST.CreateArray(dataGridView1, Arr);
            ST.InternalSort(Arr);
            ST.CheckArray(dataGridView2, Arr, square);
        }
    }
}
