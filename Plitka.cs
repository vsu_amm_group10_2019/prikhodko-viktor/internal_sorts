﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public class Plitka
    {
        public int _Width { get; set; }
        public int _Height { get; set; }
        public int _Price { get; set; }
        public int _Count { get; set; }
        public Plitka()
        {
            _Width = 0;
            _Height = 0;
            _Price = 0;
            _Count = 0;
        }

        public Plitka(int width, int height, int price, int count)
        {
            _Width = width;
            _Height = height;
            _Price = price;
            _Count = count;
        }

        

    }
}
